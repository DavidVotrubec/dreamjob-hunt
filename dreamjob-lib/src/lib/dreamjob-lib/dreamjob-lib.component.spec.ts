import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DreamjobLibComponent } from './dreamjob-lib.component';

describe('DreamjobLibComponent', () => {
  let component: DreamjobLibComponent;
  let fixture: ComponentFixture<DreamjobLibComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DreamjobLibComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(DreamjobLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
