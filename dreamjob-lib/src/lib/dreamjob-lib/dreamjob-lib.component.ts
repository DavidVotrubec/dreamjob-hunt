import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'org-dreamjob-lib',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './dreamjob-lib.component.html',
  styleUrl: './dreamjob-lib.component.css',
})
export class DreamjobLibComponent {}
