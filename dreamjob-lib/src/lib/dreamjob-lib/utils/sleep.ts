/**
 * Sleep for X miliseconds
 */
export const sleep = (ms: number) => {
  return new Promise(resolve => setTimeout(resolve, ms));
}
