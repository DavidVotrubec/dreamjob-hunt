export function bytesToMegaBytes(value: number): string {
  return `${value / 1000000}MB`;
}
