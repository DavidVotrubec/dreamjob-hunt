export type GenerateRandomArrayOptions = {
  size: number;
  /**
   * Applies to both positive and negative extremes
   */
  maxValue: number;
}


export function generateLargeRandomArray(options: GenerateRandomArrayOptions): number[] {
  const array = new Array(options.size);
  for (let i = 0; i < options.size; i++) {
    // Generates numbers from -maxValue to +maxValue
    array[i] = Math.floor(Math.random() * (2 * options.maxValue + 1)) - options.maxValue;
  }
  return array;
}
