import { bytesToMegaBytes } from "./bytes";

export function testAlgorithmEfficiency(name: string, algorithm: (arr: number[]) => void, array: number[]): void {
  console.log(`Testing ${name}`);

  const startTime = performance.now();
  const memoryUsageBefore = process.memoryUsage().heapUsed;

  algorithm(array);

  const endTime = performance.now();
  const memoryUsageAfter = process.memoryUsage().heapUsed;

  const ellapsedMs = endTime - startTime;
  const memoryUsed = memoryUsageAfter - memoryUsageBefore;

  console.log(`${name} took ${ellapsedMs} milliseconds`);
  console.log(`${name} used ${memoryUsed} bytes of additional memory. Which is ${bytesToMegaBytes(memoryUsed)}`);
}
