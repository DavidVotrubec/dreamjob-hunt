/**
 * Compare various implementations for the most common sorting algorithms
 */

import { quickSort as quickSortNotOptimized } from "../chat-gpt-study/quick-sort-not-optimized";
import { mergeSort as mergeSortNotOptimized } from "../chat-gpt-study/merge-sort-not-optimized";
import { quickSort } from "../chat-gpt-study/quick-sort";
import { mergeSort } from "../chat-gpt-study/merge-sort";
import { generateLargeRandomArray } from "../utils/arrays";
import { testAlgorithmEfficiency } from "../utils/test-algo-efficiency";
import { sleep } from "../utils/sleep";

async function measurrePerf() {
  const inputArrOrig = generateLargeRandomArray({
    maxValue: 10000,
    size: 1000 * 1000 * 10
  });

  // Prepare copies of the original array, so the subsequent runs are not affected by input array modification
  const arrayA = [...inputArrOrig];
  const arrayB = [...inputArrOrig];
  const arrayC = [...inputArrOrig];
  const arrayD = [...inputArrOrig];

  console.log(`\n`);
  console.log(`\n`);
  await testAlgorithmEfficiency('quick-sort-not-optimized', quickSortNotOptimized, arrayA);

  if (globalThis.gc) {
    globalThis.gc();
  }

  await sleep(1000);

  console.log(`\n`);
  console.log(`\n`);
  function compareFn(a: number, b: number): number {
    return a - b;
  }
  testAlgorithmEfficiency('quick-sort', (arr: number[]) => {
    quickSort(arr, compareFn);
  }, arrayB);

  await sleep(1000);
  console.log(`\n`);
  console.log(`\n`);
  testAlgorithmEfficiency('merge-sort-not-optimized', mergeSortNotOptimized, arrayC);

  await sleep(1000);
  console.log(`\n`);
  console.log(`\n`);
  testAlgorithmEfficiency('merge-sort', mergeSort, arrayD);
}

measurrePerf();
