import { quickSort } from './quick-sort';

const compareFn = (a: number, b: number) => a - b;

it('should work with empty array', () => {
  expect(quickSort([], compareFn)).toEqual([]);
})

it('should work with single item array', () => {
  expect(quickSort([1], compareFn)).toEqual([1]);
})

it('should work with sorted array of integers', () => {
  const arr = [1, 2, 4, 6, 90, 789];
  const expected = [1, 2, 4, 6, 90, 789];
  expect(quickSort(arr, compareFn)).toEqual(expected);
})

it('should work with reversly sorted array of integers', () => {
  const arr = [789, 90, 6, 4, 2, 1];
  const expected = [1, 2, 4, 6, 90, 789];
  expect(quickSort(arr, compareFn)).toEqual(expected);
})

it('should work with random array of integers', () => {
  const arr = [1, 2, 4, 0, 34, 6, -90, 789, -2, 3, 8, 8, 5, 8, 123456];
  const expected = [-90, -2, 0, 1, 2, 3, 4, 5, 6, 8, 8, 8, 34, 789, 123456];
  expect(quickSort(arr, compareFn)).toEqual(expected);
})

it('should work with random array of strings', () => {
  const compareStrFn = (a: string, b: string) => {
    if (a > b) return 1;
    if (a < b) return -1;
    return 0;
  };

  const arr = ['g', 'b', 'c', 'f', 'a', 'b'];
  const expected = ['a', 'b', 'b', 'c', 'f', 'g'];
  expect(quickSort(arr, compareStrFn)).toEqual(expected);
})

