import { mergeSort } from './merge-sort';

it('should work with empty array', () => {
  expect(mergeSort([])).toEqual([]);
})

it('should work with single item array', () => {
  expect(mergeSort([1])).toEqual([1]);
})

it('should work with simple array', () => {
  expect(mergeSort(['b', 'c', 'f', 'a'])).toEqual(['a', 'b', 'c', 'f']);
})

it('should work with longer array with duplicates', () => {
  expect(mergeSort(['1', 'c', 'f', 'a', 'b', 'x', 'y', 'b'])).toEqual(['1', 'a', 'b', 'b', 'c', 'f', 'x', 'y']);
})

it('should work with random array of integers', () => {
  const arr = [1, 2, 4, 0, 34, 6, -90, 789, -2, 3, 8, 8, 5, 8, 123456];
  const expected = [-90, -2, 0, 1, 2, 3, 4, 5, 6, 8, 8, 8, 34, 789, 123456];
  expect(mergeSort(arr)).toEqual(expected);
})
