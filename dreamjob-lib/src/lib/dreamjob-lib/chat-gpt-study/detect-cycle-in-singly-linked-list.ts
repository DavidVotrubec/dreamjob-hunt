
/**
 * Question: Detect a Cycle in a Linked List
Problem Statement:
Given a singly linked list, determine if it contains a cycle. A cycle occurs if a node’s next pointer points back to a previous node in the list. This can make a portion of the list cyclic, meaning that traversing the list indefinitely is possible.

Requirements:

You should provide a function that takes the head of the linked list as an input and returns a boolean indicating whether there is a cycle.
Try to solve this problem using O(1) space complexity, meaning you should not use extra space proportional to the number of nodes in the list.
This question tests your ability to manage pointers and understand advanced concepts in linked list operations. It is a common problem in interviews that checks for a grasp of Floyd's Tortoise and Hare cycle detection algorithm.

Please describe how you would approach this problem and then proceed to implement your solution.
*/

export type LinkedListItem<T> = {
  value: T;
  key: string;
  next: LinkedListItem<T> | undefined;
}

/**
 * We expect that each value is unique in the LinkedList, otherwise the comparison would not work
 */
export function detectCycleInSingleLinkedList<T>(headNode: LinkedListItem<T>): boolean {
  if (!headNode) {
    return false;
  }

  if (headNode.next === undefined) {
    return false;
  }

  // Use the turtle and hare approach
  let turtleNode: LinkedListItem<T> | undefined = headNode.next;
  let hareNode: LinkedListItem<T> | undefined = headNode.next?.next;

  // iterate until the value is the same
  // when that happens it means we have found a cycle
  while (turtleNode && hareNode) {
    if (turtleNode.value === hareNode.value) {
      // cycle detected
      return true;
    }

    turtleNode = turtleNode.next;
    hareNode = hareNode.next?.next;
  }

  return false;
}
