import { fibonacci } from './fibonacci';

it('should work with 0', () => {
  expect(fibonacci(0)).toBe(0);
})

it('should work with 1', () => {
  expect(fibonacci(1)).toBe(1);
})

it('should work with 2', () => {
  expect(fibonacci(2)).toBe(1);
})

it('should work with 3', () => {
  expect(fibonacci(3)).toBe(2);
})

it('should work with 4', () => {
  expect(fibonacci(4)).toBe(3);
})

it('should work with 5', () => {
  expect(fibonacci(5)).toBe(5);
})

it('should work with 6', () => {
  expect(fibonacci(6)).toBe(8);
})

it('should work with 7', () => {
  expect(fibonacci(7)).toBe(13);
})

it('should work with 8', () => {
  expect(fibonacci(8)).toBe(21);
})

it('should work with 9', () => {
  expect(fibonacci(9)).toBe(34);
})

it('should work with 10', () => {
  expect(fibonacci(10)).toBe(55);
})
