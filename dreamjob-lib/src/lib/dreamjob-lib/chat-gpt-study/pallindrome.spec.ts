import { isPallindrome } from './pallindrome';

it('should handle undefined', () => {
  expect(isPallindrome(undefined)).toBe(true);
})

it('should handle empty strings', () => {
  expect(isPallindrome('')).toBe(true);
  expect(isPallindrome(' ')).toBe(true);
  expect(isPallindrome('   ')).toBe(true);
})

it('should ignore CASE', () => {
  expect(isPallindrome('aaAA')).toBe(true);
})

it('should ignore space before and after', () => {
  expect(isPallindrome('     aaAA ')).toBe(true);
})

it('should work with words', () => {
  expect(isPallindrome('     saippuakivikauppias ')).toBe(true);
  expect(isPallindrome('racecar')).toBe(true);
  expect(isPallindrome('civic')).toBe(true);
})

it('should work with sentences', () => {
  expect(isPallindrome('deed mom deed')).toBe(true);
})

