
// This is the OPTIMIZED quickSort
// with custom comparer
// and iterative approach with explicit stack to simulate recursion
// and avoid the deep recursion stack-overflow problem.
// Returns the mutated array.
export function quickSort<T>(arr: T[], compareFn: (a: T, b: T) => number): T[] {
  if (!arr || !arr.length || arr.length === 1) {
    return arr;
  }

  // Create the explicit stack of partions
  const stack: { low: number, high: number }[] = [
    { low: 0, high: arr.length - 1 }
  ];

  // Simulation of recursion
  while (stack.length) {
    const currentStack = stack.pop()!;

    // Repeat until we reach the end of array
    if (currentStack.low < currentStack.high) {
      const nextLow = mergeSortPartOfArray(arr, currentStack.low, currentStack.high, compareFn);

      // I've basically "split" the array in left and right parts, so now I need to process both parts
      stack.push({ low: currentStack.low, high: nextLow - 1 }); // <-- here we have all the smaller items
      stack.push({ low: nextLow + 1, high: currentStack.high }); // <-- here we have all the bigger items
    }
  }

  return arr;
}


function mergeSortPartOfArray<T>(wholeArr: T[], lowIndex: number, highIndex: number, compareFn: (a: T, b: T) => number): number {
  // Use last item in our partition as the pivot
  const pivot = wholeArr[highIndex];

  // "i" marks the last index of items which are smaller then pivot
  // In effect it is the same as holding the leftPart and rightPart arrays in the not-optimized version.
  let i = lowIndex;

  // Iterate out partition
  for (let j = lowIndex; j < highIndex; j++) {
    // Check if current item is smaller then Pivot
    if (compareFn(wholeArr[j], pivot) < 0) {
      // Switch items in-place to avoid declaring additional array
      // and thus optimize memory-usage
      // if j==i, it marks the point where we have items smaller then pivot
      [wholeArr[j], wholeArr[i]] = [wholeArr[i], wholeArr[j]];
      i++;
    }
  }

  // Now switch the pivot the "middle", so all the items larger then pivot, are to its right side
  [wholeArr[i], wholeArr[highIndex]] = [wholeArr[highIndex], wholeArr[i]];

  // Return the "left part" of the array => those smaller then pivot
  return i;
}
