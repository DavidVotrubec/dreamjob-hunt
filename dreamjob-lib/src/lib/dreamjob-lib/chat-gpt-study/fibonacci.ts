// Use the iteration approach for efficiency and memory consumption reasons

/** Each number is the sum of preceding ones */
export function fibonacci(n: number, memo: Record<number, number> = {}): number {
  // input 0 or 1 returns 1
  if (n <= 1) {
    return n;
  }

  if (memo[n] !== undefined) {
    return memo[n];
  }

  let sum = 0;

  for (let i = 1; i <= n; i++) {
    const prevPrev = memo[i - 2] ?? 0;
    const prev = memo[i - 1] ?? 1;
    sum = prevPrev + prev;
    memo[i] = sum;
  }

  return sum;
}


// Recursion - can result in a crash
function fibonacciMemoization(n: number, memo: Record<number, number> = {}): number {
  if (n in memo) return memo[n];
  if (n <= 1) return n;

  memo[n] = fibonacciMemoization(n - 1, memo) + fibonacciMemoization(n - 2, memo);
  return memo[n];
}

// Example usage
console.log(fibonacciMemoization(10));  // Output: 55
