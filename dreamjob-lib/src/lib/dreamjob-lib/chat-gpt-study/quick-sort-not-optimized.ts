
// This would only work for primitive values which can be compared simply
// For anything more complex I would need additional compareFn
export function quickSort<T>(arr: T[]): T[] {
  if (!arr || !arr.length || arr.length === 1) {
    return arr;
  }

  const mid = Math.floor(arr.length / 2);
  const pivot = arr[mid];

  // Use "in-place partition"
  // This is not optimal for really large arrays, because it still assigns lot of memory slots
  // for these three memory slots.
  // It is better to use in-place switching of elements in the original array.
  const leftSide: T[] = [];
  const pivots: T[] = [pivot];
  const rightSide: T[] = [];
  arr.forEach((x, index) => {
    if (x < pivot) {
      leftSide.push(x);
    } else if (x > pivot) {
      rightSide.push(x);
    } else if (index != mid) {
      // Preserve duplicates
      // Same value as pivot, but in different index
      // Also the duplicates do not lead to recursive calls
      pivots.push(x);
    }
  })

  // The ...spread is not as effective as concat, because it can only use the common iterator interface,
  // while .concat can use internal array optimizations
  return Array.prototype.concat(quickSort(leftSide), pivots, quickSort(rightSide));
}
