import { mergeSortModified } from './count-array-inversions';

it('should work', () => {
  const unsortedArr = [5, 3, 2, 6, 9, -2, -24, 34, 0, 87, -5, 17, 18, 16, 19, 20, 22, 34, 55, 66, 8];
  let numberOfInversions = 0;


  function swapCallback(inc: number): void {
    numberOfInversions += inc;
  }

  const result = mergeSortModified(unsortedArr, swapCallback);
  console.log('result', result);
  console.table({ numberOfInversions });

  // just pass
  expect(true).toBe(true);
});

it('should simply work', () => {
  const unsortedArr = [1, 5, 3, 2, 6];
  let numberOfInversions = 0;

  function swapCallback(inc: number): void {
    numberOfInversions += inc;
  }

  const result = mergeSortModified(unsortedArr, swapCallback);
  console.log('result', result);
  console.table({ numberOfInversions });

  // just pass
  expect(true).toBe(true);
});


// Based on this article
// https://www.geeksforgeeks.org/inversion-count-in-array-using-merge-sort/
it('should work with example from GeeksForGeeks #1', () => {
  const unsortedArr = [8, 4, 2, 1];
  let numberOfInversions = 0;

  function swapCallback(inc: number): void {
    numberOfInversions += inc;
  }

  mergeSortModified(unsortedArr, swapCallback);

  // just pass
  expect(numberOfInversions).toBe(6);
});

// Based on this article
// https://www.geeksforgeeks.org/inversion-count-in-array-using-merge-sort/
it('should work with example from GeeksForGeeks #2', () => {
  const unsortedArr = [1, 20, 6, 4, 5];
  let numberOfInversions = 0;

  function swapCallback(inc: number): void {
    numberOfInversions += inc;
  }

  mergeSortModified(unsortedArr, swapCallback);

  // just pass
  expect(numberOfInversions).toBe(5);
});
