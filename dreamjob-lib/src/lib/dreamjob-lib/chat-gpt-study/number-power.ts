export function numberPowerNormal(x: number, nPower: number): number {
  if (nPower === 0) {
    return 1;
  }

  if (nPower > 0) {
    // return Math.pow(x, nPower);
    return recursiveTimes(x, nPower);
  } else {
    const positivePower = Math.abs(nPower);
    // return 1 / Math.pow(x, positivePower);
    return 1 / recursiveTimes(x, positivePower);
  }
}


export function numberPowerRecursive(x: number, nPower: number): number {
  if (nPower === 0) {
    return 1;
  }

  if (nPower > 0) {
    return Math.pow(x, nPower);
  } else {
    const positivePower = Math.abs(nPower);
    return 1 / Math.pow(x, positivePower);
  }
}

function recursiveTimes(x: number, nPower: number, res: number = 1): number {
  if (nPower > 0) {
    res = res * x;
    nPower--;
    return recursiveTimes(x, nPower, res);
  }
  return res;
}


// THIS WAS PROPOSED BY CHAT-GPT
// I have added the counter to check how many times it was called
export function numberPowerOptimized(x: number, n: number): number {
  if (n === 0) {
    return 1;  // Base case: any number to the power of 0 is 1
  }
  if (n < 0) {
    x = 1 / x;  // If exponent is negative, invert the base
    n = -n;     // and make the exponent positive
  }
  return powerHelper(x, n);
}

function powerHelper(x: number, n: number, counter = 1): number {
  if (n === 0) {
    return 1;
  }
  console.log(`COUNTER IS ${counter}`);

  counter++;
  const half = powerHelper(x, Math.floor(n / 2), counter);  // Recursively find the power of half n
  if (n % 2 === 0) {
    return half * half;  // If n is even, the result is half*half
  } else {
    return half * half * x;  // If n is odd, multiply one more x
  }
}
