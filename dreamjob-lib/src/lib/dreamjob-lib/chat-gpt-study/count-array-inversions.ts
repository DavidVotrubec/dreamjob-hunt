/**
 * @param swapCallback called whenever two items are swapped in order to be sorted
 */
export function mergeSortModified<T>(arr: T[], swapCallback: (count: number) => void): T[] {
  if (!arr || !arr.length || arr.length === 1) {
    return arr;
  }

  // Find the middle and split into halves
  const mid = Math.floor(arr.length / 2);
  const leftPart = arr.slice(0, mid);
  const rightPart = arr.slice(mid);

  const result: T[] = merge(mergeSortModified(leftPart, swapCallback), mergeSortModified(rightPart, swapCallback), swapCallback);
  return result;
}


function merge<T>(leftPart: T[], rightPart: T[], swapCallback: (count: number) => void): T[] {
  let i = 0, j = 0;
  const result: T[] = [];

  while (i < leftPart.length && j < rightPart.length) {
    const a = leftPart[i];
    const b = rightPart[j];

    // Here could be custom comparer function
    if (a < b) {
      result.push(a);
      i++;
    } else if (a > b) {
      // Here we are swapping the items
      // so, let's call the callback.
      // The "elementsBefore" is the number of possible inversions for current item.
      const elementsBefore = leftPart.length - i;
      swapCallback(elementsBefore);
      result.push(b);
      j++;
    } else {
      // They are equal ...
      result.push(a);
      result.push(b);
      i++;
      j++;
    }
  }

  // I need to concat the rest, because the left and right parts might not have been of equal lengths
  // and they were traversed in different "speed".
  return result
    .concat(leftPart.slice(i))
    .concat(rightPart.slice(j))
}


/////////////////////////////////////////////////
// Example of usage
/////////////////////////////////////////////////

const unsortedArr = [5, 3, 2, 6, 9, -2, -24, 34, 0, 87, -5, 17, 18, 16, 19, 20, 22, 34, 55, 66, 8];

/**
 * the number of inversions refers to how far the array is from being sorted.
 */
let numberOfInversions = 0;

function swapCallback(increment: number): void {
  numberOfInversions += increment;
}

const result = mergeSortModified(unsortedArr, swapCallback);
console.log('result', result);
console.table({ numberOfInversions });
