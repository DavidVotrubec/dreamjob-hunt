export type Triplet = [number, number, number];

function getTripletKey(triplet: Triplet): string {
  return [triplet[0], triplet[1], triplet[2]].sort().join(`_`);
}

function makeTriplet(str: string): Triplet {
  return str.split(`_`).map(x => parseInt(x)) as Triplet;
}

/**
 * Find all unique triplets in the input array, whose sum is 0.
 * Each triplet must be unique.
 */
export function threeSum(inputArr: number[]): Triplet[] {
  const tripletsFound = new Set<string>();

  let positiveArr: number[] = [];
  let negativeArr: number[] = [];
  let hasZeroes = false;

  inputArr.forEach(x => {
    if (x === 0) {
      hasZeroes = true;
    } else if (x > 0) {
      positiveArr.push(x);
    } else {
      negativeArr.push(x);
    }
  })

  positiveArr = positiveArr.sort();
  negativeArr = negativeArr.sort();

  // The possible ways of combination:
  // A) - 2 Pos, 1 Neg
  // B) - 1 Pos, 2 Neg
  // C) - 1 Pos, 1 Neg, 1 Zero, when Pos and Neg are opposites

  // First A)
  for (let i = 0; i < positiveArr.length; i++) {
    for (let j = i; j < positiveArr.length; j++) {
      const remainder = (positiveArr[i] + positiveArr[j]) * -1;
      const remainderExists = negativeArr.includes(remainder);
      if (remainderExists) {
        // We have a Tripet candidate
        // Check if it has been already found
        const key = getTripletKey([positiveArr[i], positiveArr[j], remainder]);
        if (!tripletsFound.has(key)) {
          tripletsFound.add(key);
        }
      }
    }
  }

  // Then B)
  for (let i = 0; i < negativeArr.length; i++) {
    for (let j = i; j < negativeArr.length; j++) {
      const remainder = (negativeArr[i] + negativeArr[j]) * -1;
      const remainderExists = positiveArr.includes(remainder);
      if (remainderExists) {
        // We have a Tripet candidate
        // Check if it has been already found
        const key = getTripletKey([negativeArr[i], negativeArr[j], remainder]);
        if (!tripletsFound.has(key)) {
          tripletsFound.add(key);
        }
      }
    }
  }


  // Then C)
  if (hasZeroes) {
    // We can combine opposite pairs
    // We can still speed it up a bit by making the arrays unique
    negativeArr = [...new Set(negativeArr)];
    positiveArr = [...new Set(positiveArr)];

    negativeArr.forEach(n => {
      if (positiveArr.includes(-n)) {
        // const triplet: Triplet = [n, 0, -n];
        // result.push(triplet);
        const key = getTripletKey([n, 0, -n]);
        tripletsFound.add(key);
      }
    });

    positiveArr.forEach(p => {
      const key = getTripletKey([-p, 0, p]);
      if (negativeArr.includes(-p) && !tripletsFound.has(key)) {
        tripletsFound.add(key);
      }
    })
  }

  const result: Triplet[] = [];
  tripletsFound.forEach(t => {
    result.push(makeTriplet(t));
  });


  return result;
}
