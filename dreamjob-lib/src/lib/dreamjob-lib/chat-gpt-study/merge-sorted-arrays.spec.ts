import { mergeSortedArrays } from './merge-sorted-arrays';

it('should work', () => {
  const arr1 = [1, 2, 3, 4, 7, 9, 10, 13, 22, 45, 56];
  const arr2 = [5, 6, 16, 44, 57];

  const result = mergeSortedArrays(arr1, arr2);
  const expectedResult: number[] = [
    1, 2, 3, 4, 5, 6, 7, 9, 10, 13, 16, 22, 44, 45, 56, 57
  ];

  expect(result).toEqual(expectedResult);
})
