import { threeSum } from './three-sum-problem';

it('should find triplets', () => {
  const inputArr = [0, 0, 1, 3, -9, -8, 4, -5, 0, -4, -3, 98, 23, 4, -2, -13, 12, 11, 10, 4, 87, 54, 34, 33, 8, 48, 0, 98, 99, 34, 56, 67, 87, 53, 22, 45, 44, 43, 42, 56, 8, 91, 19, 10, 12, 14, 42, -65, -42, -56, -8, -91, -19, -10, -12, -14, -42, -65];

  const triples = threeSum(inputArr);

  console.log(triples);
})
