import { detectCycleInSingleLinkedList, LinkedListItem } from './detect-cycle-in-singly-linked-list';

it('should work with undefined', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const result = detectCycleInSingleLinkedList(undefined as any);
  expect(result).toBe(false);
});

it('should work with single item list', () => {
  const head = {
    value: 'head',
    next: undefined
  } as LinkedListItem<string>;
  const result = detectCycleInSingleLinkedList(head);
  expect(result).toBe(false);
});


it('should work with ok list', () => {
  const next3 = {
    value: 'next3',
    next: undefined
  } as LinkedListItem<string>;

  const next2 = {
    value: 'next2',
    next: next3
  } as LinkedListItem<string>;

  const next1 = {
    value: 'next1',
    next: next2
  } as LinkedListItem<string>;

  const head = {
    value: 'head',
    next: next1
  } as LinkedListItem<string>;

  const result = detectCycleInSingleLinkedList(head);
  expect(result).toBe(false);
});


it('should work with list which indeed contains a cycle', () => {
  const next5 = {
    value: 'next5',
    next: undefined
  } as LinkedListItem<string>;

  const next4cycle = {
    value: 'next3', // <-- This value was already used, so this is a cycle
    next: next5
  } as LinkedListItem<string>;

  const next3 = {
    value: 'next3',
    next: next4cycle
  } as LinkedListItem<string>;

  const next2 = {
    value: 'next2',
    next: next3
  } as LinkedListItem<string>;

  const next1 = {
    value: 'next1',
    next: next2
  } as LinkedListItem<string>;

  const head = {
    value: 'head',
    next: next1
  } as LinkedListItem<string>;

  const result = detectCycleInSingleLinkedList(head);
  expect(result).toBe(false);
});
