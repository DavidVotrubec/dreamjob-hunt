export function isPallindrome(str: string | undefined): boolean {
  if (str === undefined || str === '' || str.length === 1) {
    return true;
  }

  str = str.toLowerCase().trim();
  const rev = str.split('').reverse().join('');
  return rev === str;
}
