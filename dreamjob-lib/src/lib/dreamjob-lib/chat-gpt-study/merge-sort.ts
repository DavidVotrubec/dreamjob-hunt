// /**
//  * Using string array for simplicity's sake
//  * to avoid solving lexical sorting of numbers ...
//  *
//  * It is very efficient algorithm, because of the managable O(n log n) complexity.
//  *
//  * ALGO STEPS:
//  * - DIVIDE:
//  * - First divide into halves
//  * - until only single elements remain
//  * - CONQUER:
//  * - take the small parts, and sort them
//  *
//  * This is IS OPTIMIZED because it uses almost-in-place sorting.
//  *
//  * Read more here https://medium.com/@babanin.nikitaa/merge-sort-a-beginner-friendly-guide-with-typescript-57baffdddb91
export function mergeSort<T>(arr: T[], left: number = 0, right: number = arr.length - 1): T[] {
  if (left < right) {  // Ensure the recursive division will eventually terminate
    const mid = Math.floor((left + right) / 2);
    mergeSort(arr, left, mid); // Sort the first half
    mergeSort(arr, mid + 1, right); // Sort the second half
    mergeInPlace(arr, left, mid, right); // Merge the sorted halves
    return arr;
  }

  return arr;
}

function mergeInPlace<T>(arr: T[], left: number, mid: number, right: number): void {
  const temp = [];
  let i = left;
  let j = mid + 1;
  let k = 0;

  // Copy only the necessary elements to temp array
  for (let index = left; index <= right; index++) {
    temp[index - left] = arr[index];
  }

  // ChatGPT generated code. It is hard for me to read and understand it.
  // But the unit tests are working fine.

  // Merge temp back into the original array
  while (i <= mid && j <= right) {
    if (temp[i - left] <= temp[j - left]) {
      arr[left + k] = temp[i - left];
      i++;
    } else {
      arr[left + k] = temp[j - left];
      j++;
    }
    k++;
  }

  // Copy the remaining elements of left half, if there are any
  while (i <= mid) {
    arr[left + k] = temp[i - left];
    i++;
    k++;
  }

  // Since elements in right half are already in place, no need to copy
}
