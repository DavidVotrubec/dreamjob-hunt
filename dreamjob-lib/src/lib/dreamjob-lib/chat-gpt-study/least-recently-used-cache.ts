type LinkedListItem<T> = {
  value: T;
  key: string;
  prev: LinkedListItem<T> | undefined;
  next: LinkedListItem<T> | undefined;
}

export class LeastRecentlyUsedCache<T> {
  private map = new Map<string, LinkedListItem<T>>();
  private head: LinkedListItem<T> | undefined = undefined;
  private tail: LinkedListItem<T> | undefined = undefined;

  constructor(private capacity: number) { }

  getSize(): number {
    return this.map.size;
  }

  get(key: string): T | -1 {
    const node = this.map.get(key);
    if (!node) {
      return -1;
    }

    // Move to front
    this.moveToFront(node);
    return node.value;
  }

  put(key: string, value: T): void {
    let node: LinkedListItem<T>;
    if (this.map.has(key)) {
      node = this.map.get(key)!;
      node.value = value;
      this.moveToFront(node);
    } else {
      node = { value, key, next: undefined, prev: undefined };
      if (this.map.size === this.capacity) {
        // Remove the least recently used item
        if (this.tail) {
          this.map.delete(this.tail.key);
          this.remove(this.tail);
        }
      }
      this.map.set(key, node);
      this.addToFront(node);
    }
  }

  private moveToFront(node: LinkedListItem<T>): void {
    if (node === this.head) {
      return;
    }
    this.remove(node);
    this.addToFront(node);
  }

  private addToFront(node: LinkedListItem<T>): void {
    if (!this.head) {
      this.head = this.tail = node;
    } else {
      node.next = this.head;
      this.head.prev = node;
      this.head = node;
    }
  }

  private remove(node: LinkedListItem<T>): void {
    if (node === this.head) {
      this.head = node.next;
    }
    if (node === this.tail) {
      this.tail = node.prev;
    }
    if (node.prev) {
      node.prev.next = node.next;
    }
    if (node.next) {
      node.next.prev = node.prev;
    }
  }
}
