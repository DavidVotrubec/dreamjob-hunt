import { mergeSort } from './merge-sort-not-optimized';

it('should work with empty array', () => {
  expect(mergeSort([])).toEqual([]);
})

it('should work with single item array', () => {
  expect(mergeSort(['1'])).toEqual(['1']);
})

it('should work with simple array', () => {
  expect(mergeSort(['b', 'c', 'f', 'a'])).toEqual(['a', 'b', 'c', 'f']);
})

it('should work with longer array with duplicates', () => {
  expect(mergeSort(['1', 'c', 'f', 'a', 'b', 'x', 'y', 'b'])).toEqual(['1', 'a', 'b', 'b', 'c', 'f', 'x', 'y']);
})
