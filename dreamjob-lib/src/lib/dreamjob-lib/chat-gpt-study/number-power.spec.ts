import { numberPowerNormal, numberPowerOptimized } from './number-power';

it('power 0', () => {
  expect(numberPowerNormal(10, 0)).toBe(1);
})

it('power positive', () => {
  expect(numberPowerNormal(10, 1)).toBe(10);
  expect(numberPowerNormal(10, 2)).toBe(100);
  expect(numberPowerNormal(10, 5)).toBe(100000);
})

it('power negative', () => {
  expect(numberPowerNormal(10, -1)).toBe(0.1);
  expect(numberPowerNormal(10, -2)).toBe(0.01);
  expect(numberPowerNormal(10, -5)).toBe(0.00001);
})


it('check optimized function', () => {
  const result = numberPowerOptimized(2, 1024);

})
