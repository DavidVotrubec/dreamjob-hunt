import { LeastRecentlyUsedCache } from './least-recently-used-cache';


describe('LeastRecentlyUsedCache', () => {
  it('should insert an item which is not yet in the cache', () => {
    const lru = new LeastRecentlyUsedCache<string>(2);
    lru.put('a', 'apple');
    expect(lru.get('a')).toEqual('apple');
  });

  it('should update the value when inserting an item which is already somewhere in the list', () => {
    const lru = new LeastRecentlyUsedCache<string>(2);
    lru.put('a', 'apple');
    lru.put('a', 'avocado');  // Update the value
    expect(lru.get('a')).toEqual('avocado');
  });

  it('should move the item to the front when it is accessed', () => {
    const lru = new LeastRecentlyUsedCache<string>(2);
    lru.put('a', 'apple');
    lru.put('b', 'banana');
    lru.get('a');  // Access 'a' to move it to the front
    expect(lru.get('c')).toEqual(-1);
    expect(lru.get('a')).toEqual('apple');
  });

  it('should evict the least recently used item when the capacity is exceeded', () => {
    const lru = new LeastRecentlyUsedCache<string>(2);

    lru.put('a', 'apple');
    lru.put('b', 'banana');
    lru.put('c', 'cherry');  // This should evict 'a'

    expect(lru.get('a')).toEqual(-1);
    expect(lru.get('b')).toEqual('banana');
    expect(lru.get('c')).toEqual('cherry');
  });

  it('should return -1 when retrieving an item not in the cache', () => {
    const lru = new LeastRecentlyUsedCache<string>(2);
    expect(lru.get('a')).toEqual(-1);
  });
});
