/**
 * Using string array for simplicity's sake
 * to avoid solving lexical sorting of numbers ...
 *
 * It is very efficient algorithm, because of the managable O(n log n) complexity.
 *
 * ALGO STEPS:
 * - DIVIDE:
 * - First divide into halves
 * - until only single elements remain
 * - CONQUER:
 * - take the small parts, and sort them
 *
 * This is NOT Optimized because it does not use in-place sorting,
 * and instead relies on .concat, which creates new instances of Array.
 *
 * Read more here https://medium.com/@babanin.nikitaa/merge-sort-a-beginner-friendly-guide-with-typescript-57baffdddb91
 */
export function mergeSort<T>(arr: T[]): T[] {
  if (!arr || !arr.length || arr.length === 1) {
    return arr;
  }

  // Find the middle and split into halves
  const mid = Math.floor(arr.length / 2);
  const leftPart = arr.slice(0, mid);
  const rightPart = arr.slice(mid);

  const result: T[] = merge(mergeSort(leftPart), mergeSort(rightPart));
  return result;
}


function merge<T>(leftPart: T[], rightPart: T[]): T[] {
  let i = 0, j = 0;
  const result: T[] = [];

  while (i < leftPart.length && j < rightPart.length) {
    const a = leftPart[i];
    const b = rightPart[j];

    // Here could be custom comparer function
    if (a < b) {
      result.push(a);
      i++;
    } else if (a > b) {
      result.push(b);
      j++;
    } else {
      // They are equal ...
      result.push(a);
      result.push(b);
      i++;
      j++;
    }
  }

  // I need to concat the rest, because the left and right parts might not have been of equal lengths
  // and they were traversed in different "speed".
  return result
    .concat(leftPart.slice(i))
    .concat(rightPart.slice(j))
}
