export function mergeSortedArrays(array1: number[], array2: number[]): number[] {
  const m = array1.length;
  const n = array2.length;
  let index1 = m - 1; // last element index in array1 that is part of the original sorted list
  let index2 = n - 1; // last element index in array2
  let mergeIndex = m + n - 1; // end of array1

  while (index2 >= 0) {
    if (index1 >= 0 && array1[index1] > array2[index2]) {
      // Copy the last item from array1 (which is target array, so that item would be overwritten anyway)
      // to the sorted position
      array1[mergeIndex] = array1[index1];
      index1--;
    } else {
      // simple insertion at the last position of the target array
      array1[mergeIndex] = array2[index2];
      index2--;
    }
    mergeIndex--;
  }

  return array1;
}
