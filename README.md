# David Votrubec - Dreamjob hunting

Repository for live coding during interviews, testing etc


### Running tests
Run ALL tests
run `npm test dreambjob-lib`
or 
run `npx nx test test dreambjob-lib`


run specific test:
For example:
`npx nx test dreamjob-lib --test-file=dreamjob-lib/src/lib/dreamjob-lib/chat-gpt-study/merge-sorted-arrays.spec.ts`


### Upgrade specific npm package
`npx nx migrate [package-name]`

## Run .TS files directly

Get inspiration from this command
`npx ts-node dreamjob-lib/src/lib/dreamjob-lib/perf-tests/conpare-sorting-algos.ts`
