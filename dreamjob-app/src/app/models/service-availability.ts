export enum ServiceAvailability {
  Ok = 'ok',
  Warning = 'warning',
  Danger = 'danger'
}
