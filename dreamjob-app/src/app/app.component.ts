import { Component } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NxWelcomeComponent } from './nx-welcome.component';
import { ServiceAvailabilityComponent } from './better-stack/service-availability/service-availability.component';
import { ServiceAvailability } from './models/service-availability';

@Component({
  standalone: true,
  imports: [NxWelcomeComponent, ServiceAvailabilityComponent, RouterModule],
  selector: 'org-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent {
  title = 'dreamjob-app';



  serviceAvailabilities = {
    api: {
      name: 'API',
      timestamps: [],
      totalAvailability: 100,
      records: new Array(100).fill(ServiceAvailability.Ok)
    },
    dashboard: {
      name: 'Dashboard',
      timestamps: [],
      totalAvailability: 99.9,
    },
    landingPage: {
      name: 'Landing page',
      timestamps: [],
      totalAvailability: 99.7
    }
  }

}
