import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServiceAvailability } from '../../models/service-availability';

@Component({
  selector: 'org-service-availability',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './service-availability.component.html',
  styleUrl: './service-availability.component.scss',
})
export class ServiceAvailabilityComponent {
  @Input()
  serviceName: string = '';

  @Input()
  totalAvailability: number = 0;

  @Input()
  availability: ServiceAvailability[] = [];
}
